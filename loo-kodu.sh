#!/bin/bash

# Autor: Chris Rätsepso, 2016

# Skriptimiskeeled 2.kodutöö (bash)

# Antud skripti lõppeesmärgiks on luua kasutaja poolt etteantud veebisait kasutades Apache2 veebiserverteenust.
# Skripti põhilised toimingud on paigaldada Apache2 (kui vaja), seadistada nimelahendus, kopeerida 000-default.conf fail,
# viia tehtud koopiasse sisse muudatused vastavalt kasutaja poolt etteantud veebisaidile, seejärel muuta index.html sisu vastavalt
# loodavale lehele, lubada loodud lehe kasutamine ning lõpuks teha Apache2 serverile reload.

# Exit koodid: 

# 1 - pole juurkasutajana sisse loginud;
# 2 - vale arv etteantud argumente;
# 3 - apache2 serveri paigaldamisel ilmnes viga
# 4 - selline kaust on juba olemas

# Kontrollitakse, kas tegemist on juurkasutajaga.

if [  $UID -ne 0 ]
then
   echo "Skript käivitub vaid juurkasutaja (Root) õigustes!"
   exit 1   
fi

# Kontrollitakse, kas kasutaja sisestas õige arvu argumente.

if [ $# -ne 1 ]; then
    echo "Kasutamine: $0 www.minuveebisait.ee"
    exit 2
fi

# Salvestatakse kasutaja poolt sisestatud veebisait muutujasse.

VEEBKODU=$1 

# Kontrollin apache2 serveri olemasolu ja vajadusel paigaldan selle.

type apache2 > /dev/null 2>&1

if [ $? -ne 0 ]
then
   apt-get update && apt-get install apache2 -y || echo "Apache2 installimisel ilmnes viga!" && exit 3
fi

# Luuakse /var/www/ kausta vastava veebisaidi kaust

if [ -d /var/www/$VEEBKODU ];
then
  echo "Selline kaust on juba olemas"
  exit 4
else  
   mkdir -p /var/www/$VEEBKODU
fi

# Seadistatakse nimelahendus 

echo "127.0.0.1  $VEEBKODU" >> /etc/hosts

# Tehakse /etc/apache2/sites-available/000-default.conf konfifailist koopia ja muudetakse seda konfifaili vastavalt veebisaidile

cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$VEEBKODU.conf

sed -i "/#ServerName www.example.com/c\ServerName $VEEBKODU" /etc/apache2/sites-available/$VEEBKODU.conf > /dev/null 2>&1
sed -i "/DocumentRoot \/var\/www\/html/c\DocumentRoot \/var\/www\/$VEEBKODU" /etc/apache2/sites-available/$VEEBKODU.conf

# Muudetakse index.html faili vastavalt loodud veebisaidile

cat > /var/www/$VEEBKODU/index.html << LOPP
<html>
  <body>
    <h1>Tere tulemast leheküljele $VEEBKODU !</h1>
  </body>
</html>
LOPP
# Lubatakse virtuaalserveri kasutamine

a2ensite $VEEBKODU

# Apache2 serverile tehakse reload

/etc/init.d/apache2 reload 



